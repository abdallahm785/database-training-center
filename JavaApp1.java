import java.sql.*;
import java.util.Scanner;

public class JavaApp1 {
	private static final Scanner S = new Scanner(System.in);

	private static Connection c = null;

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://localhost:3306/training_center";
			String username = "root";
			String password = "root";
			c = DriverManager.getConnection(url, username, password); // ToDo : Specify Parameters !

			String choice = "";

			do {
				System.out.println("-- MAIN MENU --");
				System.out.println("1 - Browse ResultSet");
				System.out.println("2 - Invoke Procedure");
				System.out.println("Q - Quit");
				System.out.print("Pick : ");

				choice = S.next().toUpperCase();

				switch (choice) {
				case "1": {
					browseResultSet();
					break;
				}
				case "2": {
					invokeProcedure();
					break;
				}
				}
			} while (!choice.equals("Q"));

			c.close();

			System.out.println("Bye Bye :)");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private static void browseResultSet() throws Exception {
		Statement s = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

		ResultSet rs = s.executeQuery(
				"SELECT module.ModuleID,module.Mname,session.date FROM module JOIN session on session.date = session.date"
						+ "	WHERE date BETWEEN CAST('2022-01-01' AS DATE) AND CAST('2022-12-31' AS DATE)"
						+ " and room IS NULL" + " ORDER BY date ASC;"); // ToDo : Specify Query !

		while (rs.next() == true) { // ToDo : Check ResultSet Contains Rows !

			System.out.println(rs.getString("moduleID"));// ToDo : Display ResultSet Rows !
			System.out.println(rs.getString("Mname"));
			System.out.println(rs.getDate("date"));
			System.out.println();

		}

		rs.close();

	}

	private static void invokeProcedure() throws Exception {
		// ToDo : Receive Course Code & Course Date !
		CallableStatement cs = c.prepareCall ( "{CALL schedule(courseID,date)}" );
		cs.getString("courseID");
		cs.getString("date");
		
		// ToDo : Specify CallableStatement !
	}
}
